# AWS Single Signon

<div display:"inline-block">
<img height="150px" src="./pics/aws-sts.png"><img height="150px" src="./pics/plus-2.png"><img height="150px" src="./pics/devops.png">
</div>

The intention behind this tool was to provide developers with an easy and consistent way of working with AWS session tokens via SSO. This particular implementation supports Okta using SAML.

## Problem

:grimacing:
Onboarding staff has historically required manual provisioning of user credentials across multiple AWS accounts, none of which were synced. Naturally this creates a poor user experience, becomes progressively more unsustainable to manage over time and does not scale as the business grows.

## Solution

:sunglasses:
Integrate all AWS accounts with a single signon solution. In this case SAML has been used to integrate with [Okta](https://www.okta.com/), leading to...

- Ideal user experience, users only remember one password which they already use for other business apps (AD login, HR apps, etc).
- Centralizes and automates the provisioning and maintenance of user credentials.
- Empowers teams to self-serve, they can manage access within their team whilst an external dev/ops team can manage access all teams.
- Reduces the attack vector as credentials are stored in Okta instead of existing across all accounts.
- Cross-platform, the Powershell module has been developed on Powershell Core 6 therefore runs consistently across Windows (native and core), MacOS and Linux.
- Supports MFA when enabled.
- Currently supports Okta, but can easily be improved to support any SAML provider (forms auth flow).

<img height="1500px" src="./pics/aws-sso-environment-example.png">

## How to Configure AWS for SAML

Explaining this is out of scope for this README, however there are plenty of great articles over at AWS explaining in detail what's needed...

- Initially I would recommend [this document](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_enable-console-saml.html), which explains how federated access works and how to configure your SAML identity provider.
- Next if you need further reading material on the subject, look no further than the [ocean of resources](https://aws.amazon.com/identity/saml/) at AWS.
- Finally [this post](https://wheniwork.engineering/finally-saml-command-line-access-for-aws-sts-b0e33bbba26e) shows how to integrate Google Auth as your IDP, instead of Okta.

## Setup

<img height="60px" src="./pics/setup.png">

### Modify the AWS/Okta Configs

This is an area to improve, but currently the configs for the AWS roles and the Okta endpoints all live in the Powershell module itself.

Naturally the values in place are examples only, so you need to change these in the section at the top of the Powershell module so that the values reflect your own environment.


### PreRequisites

- Powershell
- AWS Command Line Interface (CLI)
- AWS Tools for Powershell

### Windows

<img height="60px" src="./pics/os-windows.png">

- Because **Powershell** is bundled with Windows you can just use the native flavour without any setup required (version 5+ supported). If you wish to use Powershell Core you can install it by following [this guide.](https://docs.microsoft.com/en-us/powershell/scripting/setup/installing-powershell-core-on-windows?view=powershell-6)
- Setup the **AWS CLI** by following [this guide.](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
- Setup the **AWS Tools for Powershell** by following [this guide.](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-welcome.html)
- Configure your Powershell profile (see below).

### MacOS

<img height="60px" src="./pics/os-mac.png">

- Install **Powershell Core** by following [this guide.](https://docs.microsoft.com/en-us/powershell/scripting/setup/installing-powershell-core-on-macos?view=powershell-6)
- Setup the **AWS CLI** by following [this guide.](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
- Setup the **AWS Tools for Powershell** by following [this guide.](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-welcome.html)
- Configure your Powershell profile (see below).
- Configure your bash profile (see below).

### Linux

<img height="60px" src="./pics/os-linux.png">

- Install **Powershell Core** by following [this guide.](https://docs.microsoft.com/en-us/powershell/scripting/setup/installing-powershell-core-on-linux?view=powershell-6)
- Setup the **AWS CLI** by following [this guide.](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
- Setup the **AWS Tools for Powershell** by following [this guide.](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-welcome.html)
- Configure your Powershell profile (see below).
- Configure your bash profile (see below).

## Configure Powershell Profile

- In a Powershell session, type the following to locate your profile...

```powershell
PS>   $Profile
```

- Edit this file (or create it if it doesn't exist), adding the following content...

```powershell
Import-Module "[path where you cloned this repo]/AWSSSO.psm1"

function Get-TemporaryCredentialsAlias {
        Get-TemporaryCredentials -OverwriteDefaultProfile -Force -Verbose
}
Set-Alias -Name aws-okta -Value Get-TemporaryCredentialsAlias
function Switch-AccountAlias($account) {
        Switch-Account $account -Verbose
}
Set-Alias -Name aws-account -Value Switch-AccountAlias
function Switch-RoleAlias($account, $role) {
        Switch-Role $account $role -Verbose
}
Set-Alias -Name aws-role -Value Switch-RoleAlias
```

- Save the file and restart your Powershell session.

## Configure Bash Profile

**ProTip** : Backup your bash profile before screwing with it!

- Your bash profile is a hidden file normally at **~/.bash_profile**
- Edit (or create) this file and add the following content...

```bash
awsokta() {
   pwsh -Command aws-okta
}
awsrole() {
   . [path where you checked out this repo]/scripts/helpers/aws-role.sh ${1} ${2}
}
```

- Note that the 'aws-okta' alias references the Powershell alias you created earlier.
- Save the file and restart your bash session.

## Usage

<div display:"inline-block">
<img height="200px" src="./pics/powershell.jpg"><img height="200px" src="./pics/bash.png">
</div>

### Understanding Session Tokens

AWS provide a session token service (STS) which exposes API calls to assume a role, the successful result of which will return you a temporary session token. The token will have an expiry that you specify (up to 60 mins). Each time you perform an STS assume role call, you get a new token with a fresh expiry. Further reading can be found on the [AWS docs.](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_temp.html)

**NOTE** Your master session token 'aws-okta' persists in the credentials file (permanent), but the switch role session token is stored in your environment session (transient). Because of this, whenever you perform 'aws-role' if you switch shell session you will need to do 'aws-role' again in that session.

**You cannot switch roles until you have assumed a master role!**

### Session Token Flow - Master Role

<img src="./pics/aws-sso-master.png">

### Session Token Flow - Other Role

<img src="./pics/aws-sso-switch.png">

- In any shell session (Powershell native/core, bash, sh, etc) type the following...

```powershell
PS>   aws-okta
```

```bash
bash>   awsokta
```

- You will be presented with a list of IDP endpoints to select. These will be whatever you configured, the values in this file are just examples...

```text
----------------------------------------------------------------------------
Please enter the number of the Okta config you wish to use for this session;
   (0) AWS - Admin
   (1) AWS - Developer
   (2) AWS - Guest
   :
```

- Select the group most relevant to your task needs
- Enter your Okta credentials when prompted
- If all goes successful, you should see the following message...

```text
Your session creds have been stored in your default profile
   Path to profile : [/Users/[your user]/.aws/credentials]

Successfully fetched AWS session token! ^_^


AccessKeyId          Expiration          SecretAccessKey          SessionToken
-----------          ----------          ---------------          ------------
ABC123               10/5/18 0:0:0 pm    somesecretacceskey       sometokenvalue
```

- You now have a set of temp creds persisted in your local credentials file and are assumed into the master account role.
- You will likely now need to switch to the account which you want to work in.
- In any shell session (Powershell native/core, bash, sh, etc) type the following...

```powershell
PS> aws-role Staging FooRole
```

```bash
bash> awsrole Staging FooRole
```

- This will now use your master temp creds to try assume role of 'FooRole' in the 'Staging' account. If successful you should see this...

```text
Assume role action was successful : [arn:aws:iam::9876543210:role/FooRole]
```

## Troubleshooting

<img height="60px" src="./pics/facepalm.png">

- The main cause of errors comes from switching roles when the master role creds have expired. This requires you to perform 'aws-okta' again to refresh the master role creds.
- Next would be users providing incorrect provider creds (in this case AD creds).
- Finally, either the user has chosen an Octa group that they don't have access to, or are switching to a role in AWS that isn't trusted by the master role they are assumed into.
- If you receive an error saying your Windows Powershell version is too old, follow [this guide.](https://docs.microsoft.com/en-us/powershell/scripting/setup/installing-windows-powershell?view=powershell-6)

## Future Improvements

- Make the configurations (eg Okta endpoints) dynamic by querying Okta APIs.
- Extend to support broader range of provider types.
- Containerize tool (Docker) for easier distribution and setup.

## References, Inspiration & Further Reading

- [Powershell on MacOS](https://wilsonmar.github.io/powershell-on-mac/)
-[Persistent PowerShell: The PowerShell Profile](https://www.red-gate.com/simple-talk/sysadmin/powershell/persistent-powershell-the-powershell-profile/)
- [AWS PowerShell and PowerShell Profile](https://mukeshnotes.wordpress.com/tag/registeredaccounts-json/)
- [What Is the AWS Command Line Interface?](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)
- [Configuring AWS Credentials](https://docs.aws.amazon.com/sdk-for-net/v3/developer-guide/net-dg-config-creds.html)
-[Using AWS Credentials](https://docs.aws.amazon.com/powershell/latest/userguide/specifying-your-aws-credentials.html)
- [Github awslabs/awscli-aliases](https://github.com/awslabs/awscli-aliases)
- [AWS AssumeRoleWithSAML Docs](https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRoleWithSAML.html)
- [Use-STSRoleWithSAML Cmdlet](https://docs.aws.amazon.com/powershell/latest/reference/items/Use-STSRoleWithSAML.html)
- [Use-STSRole Cmdlet](https://docs.aws.amazon.com/powershell/latest/reference/items/Use-STSRole.html)
- [Okta Auth API Docs](https://developer.okta.com/docs/api/resources/authn)
