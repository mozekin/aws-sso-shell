#Requires -Version 5
Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"

####################################################################
# Change these values for your environment.
#
$SecondsToWaitBetweenMfaPolls = 3
$MaxSecondsToWaitForMfaVerification = 30
$AwsSessionTimeoutSeconds = 60 * 60 # (15mins-60mins)
$AwsMasterAccountNumber = "1234567890"
$AwsRegion = "ap-southeast-2"
$OktaApiBaseUri = 'https://example.okta.com/api/v1'
$OktaIdpBaseUri = 'https://example.okta.com/home/some-sso-idp'

enum Account
{
    Master
    Staging
}
$AwsAccounts = @{
    [Account]::Master = "1234567890";
    [Account]::Staging = "9876543210";
}
enum Role {
    Admin 
    Developer
    Guest
}
$AwsRoles = @{
    [Role]::Admin = "external-admin";
    [Role]::Developer = "external-developer";
    [Role]::Guest = "external-guest";
}
$OktaConfigurations = @( 
    [PSCustomObject]@{AppName="AWS - Admin"; RoleName="external-admin"; SamlProviderName="sso-admin"; OktaAppEndpoint="$OktaIdpBaseUri/abcdefg1234567/123" }, 
    [PSCustomObject]@{AppName="AWS - Developer"; RoleName="external-developer"; SamlProviderName="sso-developer"; OktaAppEndpoint="$OktaIdpBaseUri/hijklmn1234567/123" }, 
    [PSCustomObject]@{AppName="AWS - Guest"; RoleName="external-guest"; SamlProviderName="sso-guest"; OktaAppEndpoint="$OktaIdpBaseUri/opqrstu1234567/123" } 
)  
####################################################################




function Test-Verbose {
    [CmdletBinding()]
    param()
        [System.Management.Automation.ActionPreference]::SilentlyContinue -ne $VerbosePreference
}

function CheckRequiredModulesAreInstalled {
    Write-Verbose "Checking required modules are installed..."
    $hostIsPowershellCore = ($PSVersionTable.ContainsKey("PSEdition") -and ($PSVersionTable.PSEdition -eq "Core"))
    $moduleForCorePowershell = "AWSPowerShell.NetCore"
    $moduleForNativePowershell = "AWSPowerShell"
    $hasModuleForCorePowershell = (Get-Module -ListAvailable -Verbose:$false $moduleForCorePowershell) -ne $null
    $hasModuleForNativePowershell = (Get-Module -ListAvailable -Verbose:$false $moduleForNativePowershell) -ne $null
    
    if ($hostIsPowershellCore -and $hasModuleForCorePowershell -and $hasModuleForNativePowershell) {
        throw "You are running Powershell (Core) but have both AWS modules installed. Please run the following from a Powershell (Core) session : Uninstall-Module $moduleForNativePowershell"       
    }
    if (!$hostIsPowershellCore -and $hasModuleForCorePowershell -and $hasModuleForNativePowershell) {
        throw "You are running Powershell (native) but have both AWS modules installed. Please run the following from a Powershell (native) session : Uninstall-Module $moduleForCorePowershell"       
    }
    if ($hostIsPowershellCore -and !$hasModuleForCorePowershell) {
        throw "Missing module : [$moduleForCorePowershell]. Please run the following from a Powershell (Core) session : Install-Module $moduleForCorePowershell"
    } 
    if (!$hostIsPowershellCore -and !$hasModuleForNativePowershell) {
        throw "Missing module : [$moduleForNativePowershell]. Please run the following from a Powershell (native) session : Install-Module $moduleForNativePowershell"
    } 
}


# AWS allows you to store credential profiles in multiple locations.
# AWS profiles are named, with the 'default' profile being used when none is specified.
# AWS CLI looks up credentials in an ordered list of locations;
#   > Environment variables
#   > SDK credentials file ($HOME\AppData\Local\AWSToolkit\RegisteredAccounts.json)
#   > Shared credentials file ($home\.aws\credentials)
#   > User specified credentials file (*)
#   > ECS container credentials
#   > EC2 instance metadata
#
# For this implementation we only allow the default profile to be located in the Shared credentials file. 
# You may continue to use any named credentials in any location you wish, just not one named 'default'.
#
# Therefore we check if a default profile exists in the SDK store and exit if so.
# For further details please refer to the AWS documentation;
#    https://docs.aws.amazon.com/sdk-for-net/v3/developer-guide/net-dg-config-creds.html
function CheckIfDotNetSdkCredentialsExist {
    $pathToSdkCredsFile = "$HOME\AppData\Local\AWSToolkit\RegisteredAccounts.json"
    Write-Verbose "Checking for the presence of any default profile in the SDK credentials file..."
    Write-Verbose "   pathToSdkCredsFile : [$pathToSdkCredsFile]"
    $credProfiles = Get-AWSCredentials -ListProfileDetail | Where-Object { $_.StoreTypeName -eq "NetSDKCredentialsFile" -and $_.ProfileName -eq "default" }
    if ($credProfiles -ne $null) {
        throw "You have a default profile in your SDK store ($pathToSdkCredsFile).`nPlease rename the 'default' profile in this file to something else, or remove it.`nFor more info read the documentation in this PS module."
    }
    if (Test-Path $pathToSdkCredsFile) {
        $pathToSdkCredsContent = Get-Content $pathToSdkCredsFile
        if ($pathToSdkCredsContent.Contains('default')) {
            Write-Warning "You may have a default profile in your SDK store ($pathToSdkCredsFile).`nPlease rename any 'default' profile in this file to something else, or remove it.`nFor more info read the documentation in this PS module."
        }
    }
}

function ConvertSecureStringToUnsecure($secureString) {
    $unsecuredString = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureString))
    return $unsecuredString
}

function RequestConfigFromUser {
    Write-Host
    Write-Host "----------------------------------------------------------------------------"
    Write-Host "Please enter the number of the Okta config you wish to use for this session;"
    $configIndex = 0
    ForEach($oktaConfiguration in $OktaConfigurations) {
        $configName = $oktaConfiguration.AppName
        Write-Host "   ($configIndex) $configName"
        $configIndex++
    }
    $selectedConfigIndex = Read-Host "   "
    if ($selectedConfigIndex -eq $null) {
        throw "Failed to prompt config from user, value received was null!"
    }
    $currentConfig = $OktaConfigurations[$selectedConfigIndex] 
    if ($currentConfig -eq $null) {
        throw "Failed to match any configurations with index of value : [$selectedConfigIndex]"
    }
    Write-Host
    Write-Host "   Index            : $selectedConfigIndex" 
    Write-Host "   AppName          : $($currentConfig.AppName)" 
    Write-Host "   RoleName         : $($currentConfig.RoleName)" 
    Write-Host "   OktaAppEndpoint  : $($currentConfig.OktaAppEndpoint)" 
    Write-Host "----------------------------------------------------------------------------"
    Write-Host
    return $currentConfig
}



function RequestCredentialsFromUser {
    Write-Verbose 'Requesting credentials from the current user...'
    $credentials = Get-Credential -Message 'Please enter your Okta credentials (email and AD domain password)'
    if ($credentials -eq $null) {
        throw 'This tool cannot be used without entering your domain credentials!'
    }
    Write-Verbose 'Successfully requested credentials from the current user'
    return $credentials
}


function GetSessionTokenFromOkta([PSCredential]$credentials) {
    try {
        $oktaAuthRequestBody = @{ 
            "username" = $credentials.UserName; 
            "password" = (ConvertSecureStringToUnsecure($credentials.Password));
            "options" =  @{
                "warnBeforePasswordExpired" = $true;
                "multiOptionalFactorEnroll" = $true
            } 
        } | ConvertTo-Json

        $oktaAuthApiEndpoint = "$OktaApiBaseUri/authn"
        Write-Verbose "Attempting to post your domain creds to Okta..."
        Write-Verbose "   Okta endpoint : [$oktaAuthApiEndpoint]"
        $oktaAuthResponse = Invoke-WebRequest -Uri $oktaAuthApiEndpoint -Method Post -Body $oktaAuthRequestBody -ContentType "application/json" -SessionVariable okta
        $oktaAuthResponseJson = (ConvertFrom-Json $oktaAuthResponse.Content)
        $oktaAuthStatus = $oktaAuthResponseJson.status
        Write-Verbose "   Response : [$oktaAuthStatus]"

        if ($oktaAuthStatus -like 'MFA_REQUIRED') {
            $oktaMfaVerifyEndpoint = $oktaAuthResponseJson._embedded.factors[0]._links.verify.href
            $oktaMfaStateToken = $oktaAuthResponseJson.stateToken
            $oktaMfaRequestBody = @{ "stateToken" = $oktaMfaStateToken } | ConvertTo-Json
            Write-Verbose "Okta authn requires MFA - attempting to verify..."
            Write-Verbose "   MFA endpoint : [$oktaMfaVerifyEndpoint]"
            $currentSecondsToWaitForMfaVerification = 0
            $oktaMfaStatus = ''
            while($oktaMfaStatus -notlike "SUCCESS") {
                if ($currentSecondsToWaitForMfaVerification -ge $MaxSecondsToWaitForMfaVerification) {
                    throw "Timeout waiting for user to verify MFA (max $MaxSecondsToWaitForMfaVerification secs)"
                }
                $oktaMfaResponse = Invoke-WebRequest -Uri $oktaMfaVerifyEndpoint -Method Post -Body $oktaMfaRequestBody -ContentType "application/json" -SessionVariable okta 
                $oktaMfaResponseJson = (ConvertFrom-Json $oktaMfaResponse.Content)
                $oktaMfaStatus = $oktaMfaResponseJson.status
                Write-Host "   ...waiting for MFA verification ($oktaMfaStatus) ($currentSecondsToWaitForMfaVerification secs)"
                Start-Sleep $SecondsToWaitBetweenMfaPolls
                $currentSecondsToWaitForMfaVerification += $SecondsToWaitBetweenMfaPolls
            }
            Write-Verbose "   MFA successfully verified!"
            $oktaSessionToken = $oktaMfaResponseJson.sessionToken
        } 
        elseif ($oktaAuthStatus -like 'SUCCESS') {
            $oktaSessionToken = $oktaAuthResponseJson.sessionToken
        }
        else {
            throw "Received the following response from Okta authn : $oktaAuthStatus"
        }
        Write-Verbose "Successfully fetched session token from Okta!"
        return $oktaSessionToken

    } catch [Exception] {
        $errorMessage = $_.Exception.Message
        throw "Failed to get session token from Okta : $errorMessage"
    }
}

function GetSamlAssertion($currentConfig, $oktaSessionToken) {
    try {
        $oktaAppName = $currentConfig.AppName
        $oktaAppEndpoint = $currentConfig.OktaAppEndpoint
        Write-Verbose "Attempting to get SAML assertion..."
        Write-Verbose "   Okta endpoint : [$oktaAppEndpoint]"
        $samlAuthUri = "$oktaAppEndpoint`?onetimetoken=$oktaSessionToken"
        $samlAuthResponse = Invoke-WebRequest -Uri $samlAuthUri -Method Get -SessionVariable okta -UseBasicParsing
        $samlAssertionRaw = $samlAuthResponse.inputfields | Where-Object name -like "saml*" | Select-Object -ExpandProperty value
        if ($samlAssertionRaw -eq $null -or $samlAuthResponse.Content.Contains("Not Assigned")) {
            throw "You are not assigned to the Okta config you selected : [$oktaAppName]"
        }
        if ($samlAuthResponse.StatusCode -ne 200) {
            throw "The SAML assertion response from Okta was : [$($samlAuthResponse.StatusCode)] $($samlAuthResponse.StatusDescription)"
        }
        $samlAssertionParsed = $samlAssertionRaw.Replace("&#x2b;", "+").Replace("&#x3d;", "=")
        Write-Verbose "Successfully fetched SAML assertion from Okta!"
        return $samlAssertionParsed
    } catch [Exception] {
        $errorMessage = $_.Exception.Message
        throw "Failed to get SAML assertion from Okta : $errorMessage"
    }
}

function GetSessionCredentialsFromAws($currentConfig, $samlAssertion) {
    try {
        $roleName = $currentConfig.RoleName
        $samlProviderName = $currentConfig.SamlProviderName
        $awsAssumeRoleArn = "arn:aws:iam::$AwsMasterAccountNumber`:role/$roleName"
        $awsPrincipleArn = "arn:aws:iam::$AwsMasterAccountNumber`:saml-provider/$samlProviderName"
        Write-Verbose "Attempting to assume IAM role using SAML assertion..."
        Write-Verbose "   AWS IAM Role  : [$awsAssumeRoleArn]"
        Write-Verbose "   AWS Principle : [$awsPrincipleArn]"
        $response = Use-STSRoleWithSAML -RoleArn $awsAssumeRoleArn -PrincipalArn $awsPrincipleArn -DurationInSeconds $AwsSessionTimeoutSeconds -SAMLAssertion $samlAssertion -Region $AwsRegion
        Write-Verbose $response
        return $response.Credentials
    } catch [Exception] {
        $errorMessage = $_.Exception.Message
        throw "Failed to get session creds from AWS : $errorMessage"
    }
}


function SetAwsProfileForCredentials([Amazon.SecurityToken.Model.Credentials]$awsCredentials) {
    try {
        if (!$OverwriteDefaultProfile) {
            return
        }
        $credentialsFolder = "$HOME/.aws"
        Write-Verbose "Checking if the credentials folder exists at : [$credentialsFolder]"
        $credentialsFolderExists = Test-Path $credentialsFolder
        if (!$credentialsFolderExists) {
            Write-Warning "The credentials folder does NOT exist, attempting to create..."
            Write-Warning "   Path : [$credentialsFolderExists]"
            New-Item -Path $credentialsFolder -ItemType Directory
        }
        $credentialsFilePath = "$credentialsFolder/credentials"
        Write-Verbose "Checking if the credentials file exists at : [$credentialsFilePath]"
        $credentialsFileExists = Test-Path $credentialsFilePath
        if (!$credentialsFileExists) {
            Write-Warning "The credentials file does NOT exist, attempting to create..."
            Write-Warning "   Path : [$credentialsFilePath]"
            "[default]`nregion=$AwsRegion" | Set-Content -Path $credentialsFilePath
        }
        if (!$credentialsFileExists -and !$Force) {
            $confirmationMessage = "You have chosen to overwrite your default profile with the current session creds.`nThe profile file is located at : $credentialsFilePath"
            $confirmationQuestion = 'Are you sure you want to proceed?'
            $confirmationChoices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
            $confirmationChoices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
            $confirmationChoices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))
            $confirmationDecision = $Host.UI.PromptForChoice($confirmationMessage, $confirmationQuestion, $confirmationChoices, 1)
            if ($confirmationDecision -ne 0) {
                Write-Host 'Your default profile has not been modified.'
                return
            }
        }
        Write-Verbose "Attempting to set AWS session credentials in local profile..."
        Write-Verbose "   Path to profile : [$credentialsFilePath]"
        $awsAccessKey = $awsCredentials.AccessKeyId
        $awsSecretKey = $awsCredentials.SecretAccessKey
        $awsSessionToken = $awsCredentials.SessionToken
        Set-AWSCredential -AccessKey $awsAccessKey -SecretKey $awsSecretKey -SessionToken $awsSessionToken -StoreAs "default" -ProfileLocation $credentialsFilePath
        Write-Host 'Your session creds have been stored in your default profile'
        Write-Host "   Path to profile : [$credentialsFilePath]"
    } catch [Exception] {
        $errorMessage = $_.Exception.Message
        throw "Failed to set AWS profile : $errorMessage"
    }
}

function ClearEnvironmentCreds {
    # We clear existing creds from the environment when switching roles.
    # This ensures that when switching roles, the master role stored in the credentials file is used.
    # This simplifies the trust relationships between the various account roles.
    Write-Verbose "Clearing environment credentials..."
    $env:AWS_ACCESS_KEY_ID = $null
    $env:AWS_SECRET_ACCESS_KEY = $null
    $env:AWS_SESSION_TOKEN = $null
    $env:AWS_CURRENT_ROLE = $null
}

function ConstructRoleArn($accountNumber, $roleName) {
    "arn:aws:iam::$($accountNumber):role/$roleName"
}

function GetCurrentAccount {
    Write-Verbose "Determining current account..."
    $currentIdentity = Get-STSCallerIdentity
    if ($currentIdentity -eq $null) {
        throw "You don't have a current session identity, has your session expired?"
    }
    $currentAccount = $currentIdentity.Account
    Write-Verbose "   Current Account : [$currentAccount]"
    return $currentAccount
}

function GetCurrentRole {
    # When calling Get-STSCallerIdentity, the ARN in the response excludes any path value.
    # Because of this caveat, we persist the current role in the environment vars. 
    # arn:aws:iam::ACCOUNT:role/PATH/NAME
    Write-Verbose "Determining current role..."
    $currentRoleArn = $env:AWS_CURRENT_ROLE
    if ($currentRoleArn -eq $null) {
        Write-Verbose "   Current role is null!"
        return $null
    }
    $roleArnParts = $currentRoleArn.Split('/')
    $roleNamePart = $currentRoleArn.Replace($roleArnParts[0], '').TrimStart('/')
    Write-Verbose "   Current Role : [$roleNamePart]"
    return $roleNamePart
}

function AssumeRole($roleArn, $roleSessionName) {
    ClearEnvironmentCreds
    Write-Verbose "Attempting to assume role..."
    Write-Verbose "   RoleARN : [$roleArn]"
    Write-Verbose "   RoleSessionName : [$roleSessionName]"
    $assumeRoleResponse = Use-STSRole -RoleArn $roleArn -RoleSessionName $roleSessionName -DurationInSeconds $AwsSessionTimeoutSeconds -Region $AwsRegion
    if ($assumeRoleResponse -eq $null) {
        throw "Failed to switch role, your current creds may not have access, or the role may not allow switching to other roles"
    }
    Write-Verbose "Successfully assumed role, attempting to set environment creds..."
    $tempCreds = $assumeRoleResponse.Credentials
    $env:AWS_ACCESS_KEY_ID = $tempCreds.AccessKeyId
    $env:AWS_SECRET_ACCESS_KEY = $tempCreds.SecretAccessKey
    $env:AWS_SESSION_TOKEN = $tempCreds.SessionToken
    $env:AWS_CURRENT_ROLE = $roleArn
    Write-Host "Assume role action was successful : [$roleArn]" -ForegroundColor Green
}








# -----------------------------------------------------------------------------------------------------------------------------------------

function Get-TemporaryCredentials {
    <#
    .Synopsis
        Use this cmdlet to fetch a temporary AWS session token from Okta using your AD creds.
    .Outputs
        This cmdlet returns an Amazon.SecurityToken.Model.Credentials object containing the temporary credentials.
    .Parameter OverwriteDefaultProfile
        If flag provided, will persist the creds to the default profile in the shared credentials file ($home/.aws/credentials), overwriting the existing creds (optional).
    .Parameter Force
        If flag provided, will not prompt for confirmation when the OverwriteDefaultProfile flag is provided (optional).
    .Parameter OktaConfigIndex
        If provided, will not prompt for Okta config number. Run without this parameter first to discover available configs.
    .Example
        Get-TemporaryCredentials -OverwriteDefaultProfile -Force
    .Example
        $myTempCreds = Get-TemporaryCredentials
        # do something with the creds object
    #>
    param
    (
        [Parameter(Mandatory=$false)]
        [switch] 
        $OverwriteDefaultProfile,

        [Parameter(Mandatory=$false)]
        [switch] 
        $Force,

        $OktaConfigIndex
    )
    Write-Verbose "############################################################################"
    Write-Verbose "##                   AWS SSO Automation Script                       ##"
    Write-Verbose "############################################################################"    
    Write-Verbose "Using the following paths and repo status;"
    $currentFolder = Convert-Path .
    $scriptFolder = $PSScriptRoot
    Write-Verbose "   currentFolder : $currentFolder"
    Write-Verbose "   scriptFolder  : $scriptFolder"
    Write-Verbose "$( $(Set-Location $scriptFolder; & git status; Set-Location $currentFolder) )"
    Write-Verbose "Using the following Powershell environment;"
    Write-Verbose ($PSVersionTable | Format-Table | Out-String)
    Write-Verbose "Using the following loaded modules;"
    $installedModules = Get-Module -ListAvailable -Verbose:$false "AWS*"
    Write-Verbose ($installedModules | Format-Table | Out-String)
    Write-Verbose "Using the following parameters;"
    Write-Verbose "   OktaApiBaseUri                     : [$OktaApiBaseUri]"
    Write-Verbose "   SecondsToWaitBetweenMfaPolls       : [$SecondsToWaitBetweenMfaPolls]"
    Write-Verbose "   MaxSecondsToWaitForMfaVerification : [$MaxSecondsToWaitForMfaVerification]"
    Write-Verbose "   AwsMasterAccountNumber             : [$AwsMasterAccountNumber]"
    Write-Verbose "   AwsRegion                          : [$AwsRegion]"
    Write-Verbose "   AwsSessionTimeoutSeconds           : [$AwsSessionTimeoutSeconds]"
    Write-Verbose "   OverwriteDefaultProfile            : [$OverwriteDefaultProfile]"

    CheckRequiredModulesAreInstalled
    CheckIfDotNetSdkCredentialsExist

    if ($OktaConfigIndex -ne $null) {
        $currentConfig = $OktaConfigurations[$OktaConfigIndex] 
        if ($currentConfig -eq $null) {
            throw "Failed to match any configurations with index of value : [$OktaConfigIndex]. Run without this parameter first to discover available configs"
        }
    }
    else {
        $currentConfig = RequestConfigFromUser
    }
    
    ClearEnvironmentCreds
    $credentials = RequestCredentialsFromUser
    $oktaSessionToken = GetSessionTokenFromOkta -credentials $credentials
    $samlAssertion = GetSamlAssertion -currentConfig $currentConfig -oktaSessionToken $oktaSessionToken
    $awsSessionCredentials = GetSessionCredentialsFromAws -currentConfig $currentConfig -samlAssertion $samlAssertion
    SetAwsProfileForCredentials -awsCredentials $awsSessionCredentials
    Write-Host
    Write-Host "Successfully fetched AWS session token! ^_^"
    Write-Host
    return $awsSessionCredentials
}
Export-ModuleMember -function Get-TemporaryCredentials



function Switch-Account {
    <#
    .SYNOPSIS
        Use this cmdlet to switch to an AWS account, keeping your current role.
        NOTE that you cannot use this when your current account is master.
    .PARAMETER Account
        The name of the account to use.
    .Example
        Switch-Account -Account Master
    #>
    param
    (
        [Parameter(Mandatory=$true)]
        [Account] 
        $Account
    )
    $currentAccount = GetCurrentAccount
    $currentRole = GetCurrentRole
    ClearEnvironmentCreds

    if ($currentRole -eq $null -and $currentAccount -eq $AwsMasterAccountNumber) {
        throw "Your current account is master. Please use Switch-Role to switch to an account role."
    }
    if ($currentRole -eq $null) {
        throw "Something is wrong! Your current role is null and your current account is [$currentAccount]"
    }

    $selectedAccount = $AwsAccounts[$Account]
    
    $roleArn = ConstructRoleArn -accountNumber $selectedAccount -roleName $currentRole
    $roleSessionName = "$Account`_$currentRole".Replace('/', '_')
    AssumeRole -roleArn $roleArn -roleSessionName $roleSessionName
}
Export-ModuleMember -function Switch-Account

function Switch-Role {
    <#
    .SYNOPSIS
        Use this cmdlet to switch to an AWS account and IAM role.
    .PARAMETER Account
        The name of the account to use.
    .PARAMETER Role
        The name of the role to use.
    .Example
        Switch-Role -Account Master -Role Admin
    #>
    param
    (
        [Parameter(Mandatory=$true)]
        [Account]
        $Account,

        [Parameter(Mandatory=$true)]
        [Role] 
        $Role
    )
    ClearEnvironmentCreds

    $selectedAccount = $AwsAccounts[$Account]
    $selectedRole = $AwsRoles[$Role]

    $roleArn = ConstructRoleArn -accountNumber $selectedAccount -roleName $selectedRole
    $roleSessionName = "$Account`_$Role"
    AssumeRole -roleArn $roleArn -roleSessionName $roleSessionName
}
Export-ModuleMember -function Switch-Role

