#!/bin/bash

#---------------------------------------------------------------------------------------------
# NOTES
#   Requires; 
#   - jq (https://stedolan.github.io/jq/download/)
#
#---------------------------------------------------------------------------------------------

declare -a ACCOUNT_NAMES=("master" "staging")
declare -a ACCOUNT_NUMBERS=("1234567890" "9876543210")
declare -a ROLE_NAMES=("Admin" "Developer" "Guest")
declare -a ROLE_ARNS=("external-admin" "external-developer" "external-guest")


get_account_number() {
    local ACCOUNT_NAME=${1}
    for i in "${!ACCOUNT_NAMES[@]}"; do
        local THIS_ACCOUNT_NAME=${ACCOUNT_NAMES[$i]}
        local THIS_ACCOUNT_NUMBER=${ACCOUNT_NUMBERS[$i]}
        if [[ "${THIS_ACCOUNT_NAME}" = "${ACCOUNT_NAME}" ]]; then
            echo "${THIS_ACCOUNT_NUMBER}";
            return
        fi
    done
}

get_role_arn() {
    local ACCOUNT_NUMBER=${1}
    local ROLE_NAME=${2}
    for i in "${!ROLE_NAMES[@]}"; do
        local THIS_ROLE_NAME=${ROLE_NAMES[$i]}
        local THIS_ROLE_ARN=${ROLE_ARNS[$i]}
        if [[ "${THIS_ROLE_NAME}" = "${ROLE_NAME}" ]]; then
            echo "arn:aws:iam::${ACCOUNT_NUMBER}:role/${THIS_ROLE_ARN}";
            return
        fi
    done
}

clear_current_env() {
    unset AWS_DEFAULT_REGION
    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN
    unset AWS_CURRENT_ACCOUNT
    unset AWS_CURRENT_ROLE
} 

assume_role() {
    SWITCH_ROLE_ARN=${1}
    SWITCH_ROLE_RESPONSE=$(aws sts assume-role --role-arn ${SWITCH_ROLE_ARN} --role-session-name temp-assumed-role)
    AWS_ACCESS_KEY_ID=$(echo $SWITCH_ROLE_RESPONSE | jq '.["Credentials"] | .["AccessKeyId"]' | tr -d '"' )
    AWS_SECRET_ACCESS_KEY=$(echo $SWITCH_ROLE_RESPONSE | jq '.["Credentials"] | .["SecretAccessKey"]' | tr -d '"' )
    AWS_SESSION_TOKEN=$(echo $SWITCH_ROLE_RESPONSE | jq '.["Credentials"] | .["SessionToken"]' | tr -d '"' )

    export AWS_DEFAULT_REGION="ap-southeast-2"
    export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
    export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    export AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN}
    export AWS_CURRENT_ACCOUNT=${SWITCH_ROLE_ARN}
    export AWS_CURRENT_ROLE=${SWITCH_ROLE_ARN}
}

GIVEN_ACCOUNT_NAME=${1}
GIVEN_ROLE_NAME=${2}

echo
echo
echo
echo "Getting account number..."
echo "   GIVEN_ACCOUNT_NAME=${GIVEN_ACCOUNT_NAME}"
echo "   GIVEN_ROLE_NAME=${GIVEN_ROLE_NAME}"
ACCOUNT_NUMBER=$( get_account_number ${GIVEN_ACCOUNT_NAME} )
echo "   ACCOUNT_NUMBER=${ACCOUNT_NUMBER}"
if [ -z ${ACCOUNT_NUMBER} ]; then
    echo "   [ERROR] Invalid account name provided (${GIVEN_ACCOUNT_NAME}) - Supported values are;" >&2
    echo "   ${ACCOUNT_NAMES[@]}" >&2
    echo
    return
fi

echo
echo "Getting role arn..."
echo "   GIVEN_ACCOUNT_NAME=${GIVEN_ACCOUNT_NAME}"
echo "   GIVEN_ROLE_NAME=${GIVEN_ROLE_NAME}"
ROLE_ARN=$( get_role_arn ${ACCOUNT_NUMBER} ${GIVEN_ROLE_NAME} )
if [ -z ${ROLE_ARN} ]; then
    echo "   [ERROR] Invalid role name provided (${GIVEN_ROLE_NAME}) - Supported values are;" >&2
    echo "   ${ROLE_NAMES[@]}" >&2
    echo
    return
fi
echo "   CURRENT_ROLE_ARN=$AWS_CURRENT_ROLE"
echo "   ROLE_ARN=${ROLE_ARN}"

echo
echo "Clearing current env..."
clear_current_env
env | grep AWS

echo
echo "Attempting to assume role..."
echo "   ROLE_ARN=${ROLE_ARN}"
assume_role ${ROLE_ARN}

echo
echo "------------------------------"
aws sts get-caller-identity
echo "------------------------------"
echo "Successfully assumed role : ${ROLE_ARN}"
